<footer>
    <a href="#header" class="footer-top">Вверх</a>
    <ul class="footer-menu footer-menu_navigation">
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="#header">Главная</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="#about">О нас</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="#services">Услуги</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="#ships">Суда</a>
        </li>
    </ul>

    <ul class="footer-menu">
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userVk]');?>">ВКОНТАКТЕ</a>
        </li>
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userFacebook]');?>">FACEBOOK</a>
        </li>
        <li class="footer-menu__item footer-menu__item_center">
            <a class="footer-menu__link" href="<?=do_shortcode('[userInstagram]');?>">INSTAGRAM</a>
        </li>
    </ul>

    <div class="infobox">
        <div class="infobox__ultramarin">
            <a href="/"><img src="<?=get_template_directory_uri();?>/assets/images/logo_footer.png" alt="logo"></a>
            <p class="infobox__company-name">ULTRAMARIN RUSSIA</p>
        </div>
        <p class="story__text">DEVELOP BY</p>

        <div class="infobox__develop">
            <a target="_blank" href="https://www.facebook.com/bloodborne.gothic/">
                <img class="infobox__emblem" src="<?=get_template_directory_uri();?>/assets/images/my-logo.png" alt="img">
            </a>
        </div>
    </div>

    <p class="copyright">Ultramarin Russia 2020. All rightes reserved.</p>
</footer>

<?php wp_footer(); ?>

</body>
</html>