<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ultramarin</title>

    <link rel="icon" href="<?=get_template_directory_uri();?>/assets/images/favicon.ico" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php wp_head(); ?>
</head>

<body>
    <header id="header">
        <div class="topmenu">
            <a href="/">
                <img class="topmenu__logo" src="<?=get_template_directory_uri();?>/assets/images/logo.png" alt="img">
            </a>

            <ul class="topmenu__list">
                <li class="topmenu__item">
                    <a class="topmenu__link" href="#about">О нас</a>
                </li>
                <li class="topmenu__item">
                    <a class="topmenu__link" href="#services">Услуги</a>
                </li>
                <li class="topmenu__item">
                    <a class="topmenu__link" href="#ships">Суда</a>
                </li>
                <li class="topmenu__item">
                    <a class="topmenu__link" href="#contacts">Контакты</a>
                </li>
            </ul>

            <img id="menu" class="topmenu__button" src="<?=get_template_directory_uri();?>/assets/images/button.png" alt="button">
        </div>

        <div class="tagline">
            <h1 class="tagline__title">Ultramarin Russia</h1>
            <p class="tagline__text">Стань частью команды уже сегодня!</p>
            <a class="tagline__link tagline__link_header" href="#services">Подробнее</a>
        </div>
    </header>