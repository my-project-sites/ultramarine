<?php 
	get_header(); 
	wp_reset_postdata();
?>

    <div id="about" class="story">
        <h2 class="story__title">О НАС</h2>
        <h3 class="story__subtitle">СФЕРА ДЕЯТЕЛЬНОСТИ ULTRAMARIN RUSSIA: КРЮИНГОВЫЙ МЕНЕДЖМЕНТ</h3>
        <p class="story__text">Мы предоставляем услуги по подбору и найму высококвалифицированных моряков в соответствии с высокими требованиями наших клиентов. Наша компания сертифицирована DNV GL согласно требованиям ISO и MLC. Персонал компании имеет большой опыт работы в сфере крюинга. Наша компания имеет тесные связи с морскими учебными заведениями стран Балтии, России и Украины.</p>

        <p class="story__text story__text_slogan">МЫ ЗАБОТИМСЯ О НАШИХ ЛЮДЯХ НА БОРТУ СУДНА И НА БЕРЕГУ.</p>
        <p class="story__text">Для получения более подробной информации вы можете связаться с руководством компании.</p>
        <a class="tagline__link" href="#contacts">Подробнее</a>
    </div>

    <div class="posterbox">
        <div class="posterbox__poster">
            <img src="<?=get_template_directory_uri();?>/assets/images/services_1.jpg" alt="img">
            <h4 class="posterbox__title">Трудоустройство моряков</h4>
        </div>

        <div class="posterbox__poster">
            <img src="<?=get_template_directory_uri();?>/assets/images/services_2.jpg" alt="img">
            <h4 class="posterbox__title">Полный набор состава</h4>
        </div>

        <div class="posterbox__poster">
            <img src="<?=get_template_directory_uri();?>/assets/images/services_3.jpg" alt="img">
            <h4 class="posterbox__title">Помощь при обращении</h4>
        </div>
    </div>

    <div id="services" class="story story_white story_services">
        <h2 class="story__title story__title_black-line">Услуги</h2>
        <h3 class="story__subtitle">Набираем экипажи судов рядового и комсостава на суда зарубежного и российского флага:</h3>
        <ul class="story__list">
            <li class="story__item">Сухогрузы</li>
            <li class="story__item">Танкерный флот</li>
            <li class="story__item">Оффшорный флот</li>
            <li class="story__item">Рыбопромысловый флот</li>
        </ul>
        <p class="story__text">Мы предлагаем рабочие места для офицеров и рядового состава как палубной, так и машинной команды для работы на борту современных нефтяных, продуктовых и химических танкеров,сухогрузы,оффшорный флот,контейнеровозы. Полные наборы экипажев судов.</p>
        <p class="story__text story__text_slogan">Вопросы по сотрудничеству присылайте на нашу на почту:</p>
        <a class="tagline__link tagline__link_white" href="#contacts">Подробнее</a>
    </div>

    <div id="ships" class="employment-box">
        <h2 class="story__title">Трудоустройство на суда:</h2>
        <div class="employment-box__employment">
            <div class="employment-box__ship">
                <img src="<?=get_template_directory_uri();?>/assets/images/ship_1.jpg" alt="ship">
                <h4 class="posterbox__title posterbox__title_container-ship">Контейнеровозы</h4>
            </div>

            <div class="employment-box__ship">
                <img src="<?=get_template_directory_uri();?>/assets/images/ship_2.jpg" alt="ship">
                <h4 class="posterbox__title">Оффшорный флот</h4>
            </div>
        </div>

        <h2 class="story__title story__title_employment">А также:</h2>
        <div class="employment-box__employment">
            <div class="employment-box__ship">
                <img src="<?=get_template_directory_uri();?>/assets/images/ship_3.jpg" alt="ship">
                <h4 class="posterbox__title posterbox__title_container-ship">Пассажирские суда</h4>
            </div>

            <div class="employment-box__ship">
                <img src="<?=get_template_directory_uri();?>/assets/images/ship_4.jpg" alt="ship">
                <h4 class="posterbox__title posterbox__title_container-ship">Танкерный флот</h4>
            </div>
        </div>
    </div>

    <div id="contacts" class="contact">
        <h2 class="story__title story__title_black-line">Контакты</h2>
        <div class="contact__box">
            <div class="contact__info">
                <h3 class="story__subtitle">Ответы на вопросы:</h3>
                <p class="story__text">С удовольствием ответим на все ваши вопросы по телефону <?=do_shortcode('[userPhone]');?></p>
                <p class="story__text">Или просто заполните форму ниже:</p>
            </div>
            <div class="contact__info">
                <h3 class="story__subtitle story__subtitle_right">Центральный офис:</h3>
                <p class="story__text story__text_right">Россия, г.Калининград, ул.Генерал-Лейтенанта Озерова, д.17 Б</p>
                <p class="story__text story__text_right"><?=do_shortcode('[userPhone]');?></p>
            </div>
        </div>

        <div class="contact__resume">
            <?=do_shortcode('[wpforms id="14"]');?>
            <h3 class="story__subtitle">Хотите у нас работать?</h3>
            <p class="story__text">Пожалуйста, отправьте резюме и сопроводительное письмо по адресу:</p>
            <a class="story__email" href="mailto:ultramarin.russia@mail.ru">ultramarin.russia@mail.ru</a>
        </div>
    </div>

<?php get_footer(); ?>