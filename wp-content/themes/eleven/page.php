<?php 
	get_header('private'); 
	wp_reset_postdata();
?>

<div id="content">
    <?php the_content(); ?>
</div>

<?php get_footer('private'); ?>