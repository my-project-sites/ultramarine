<?php

# Имя БД
define('DB_NAME', 'ultramarine');

# Имя пользователя MySQL
define('DB_USER', 'root');

# Пароль к базе данных MySQL
define('DB_PASSWORD', '');

# Имя сервера MySQL
define('DB_HOST', 'localhost');

# Кодировка базы данных для создания таблиц.
define('DB_CHARSET', 'utf8mb4');

# Схема сопоставления. Не меняйте, если не уверены.
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '+t{Yv%u^2UZY[9gEh^t12a~-<eZpft)%&5Ny*HtU[)|Avw>vZhvNf+#)pWo*&DmX');
define('SECURE_AUTH_KEY',  '-:Vz`Eot+&]c*e%EQfN-6jxIo.&O/c{pkX]<GAc|ww-Ukouv5yV_KfTX`iS_>1c;');
define('LOGGED_IN_KEY',    ':1DSiX,iIK*XVsl&x|.On@HmmeIu7lK^uX|8X(&(EfIH>nlM8^nrAnr,QkPAgmg@');
define('NONCE_KEY',        'NbMx(77zq9bJz<r@-J,0>&.QJ=Vpu$JMw~PX:j%m%%d|a(k-*kTTO?H)La]qq*r-');
define('AUTH_SALT',        '-b*.%BIkJ5&z8B;/L[VqWe1k?{}V ci62Q8I@>3s%xqtf>2Je?;wjVtTBDvcY57m');
define('SECURE_AUTH_SALT', 'OwU06ov{G9PZbOHi%B|/c&uIvW!#;|20^4j9.~/QyRga^~4OOcz8D3yV=NWwh;Y;');
define('LOGGED_IN_SALT',   'zb&5EA:_~z+%.nq,&.(`;w1 Zq!w3 4<3qTF%QN;M!AkXK$C`T==*n&5hB[N/~[9');
define('NONCE_SALT',       '-+TgL4Tb-gd+{z.ZN,86,WN!1~Y}YRENO|KVlO;o5_]W(>un[}|b22:hv<.^&}LL');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */

$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define('WP_DEBUG', false);

# Это всё, дальше не редактируем. Успехов!

# Абсолютный путь к директории WordPress.
if ( ! defined('ABSPATH' ) ) {
	define('ABSPATH', dirname( __FILE__ ) . '/');
}

#Инициализирует переменные WordPress и подключает файлы.
require_once(ABSPATH . 'wp-settings.php');