// Подключение пакетов
var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var cleanCss = require('gulp-clean-css');
var browserSync = require('browser-sync').create();
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');

const THEME_DIR = './wp-content/themes/eleven/'

var config = {
     
    input: {
        php: THEME_DIR + '**/*.php',
        less: THEME_DIR + 'assets/less/main.less'
    },
    output: {
        css: THEME_DIR + 'assets/css/'
    },
    compilation: {
        less: THEME_DIR + 'assets/**/*.less'
    }
};

gulp.task('less', function() {
    return  gulp.src(config.input.less)
            .pipe(plumber({
                errorHandler: notify.onError(function(err) {
                    return {
                        title: 'Error',
                        message: err.message
                    }
                })
            }))
            .pipe(sourcemaps.init())
            .pipe(less())
            .pipe(autoprefixer({
                browsers: ['last 3 versions'],
                cascade: false 
            }))
            .pipe(cleanCss())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(config.output.css))
            .pipe(browserSync.stream());
});

gulp.task('server', ['less'], function() {
    browserSync.init({
        proxy: "ultramarin",
        open : false,
        notify : false,
        ghostMode : false,
        ui: {
            port: 8001
        }
    });

    gulp.watch(config.input.php).on('change', browserSync.reload);
    gulp.watch(config.compilation.less, ['less']);

});

gulp.task('go', ['server']);